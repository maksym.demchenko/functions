const getSum = (str1, str2) => {
  // add your implementation below
  let result;

  if(typeof(str1) === 'string' && typeof(str2) === 'string'){
  if(str1.length === 0){
      str1 = '0';
  }
  if(str2.length === 0){
      str2 = '0';
  }

  if(isNaN(parseInt(str1)) || isNaN(parseInt(str2))){
      return false;
  } else {
      result = parseInt(str1) + parseInt(str2);
  } 
  } else{
  return false;
  }

  
  return '' + result;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let counterPost = 0;
  let counterComments = 0;
  let result;

  for(let i = 0; i < listOfPosts.length; i++){
      if(listOfPosts[i].author === authorName){
          counterPost += 1;
      } 
      
      if(listOfPosts[i].comments){
          for(let j = 0; j < listOfPosts[i].comments.length; j++){
              if(listOfPosts[i].comments[j].author === authorName){
                  counterComments += 1;
              }
          }
      }
  }

  return ('Post:'+counterPost+',comments:'+counterComments);
};

const tickets=(people)=> {
  // add your implementation below
  let priceBillet = 25;
  let balance = 0;
  
  for(let i = 0; i < people.length; i++){
      priceBillet
      if(typeof(people[i]) === 'string');
      people[i] = parseInt(people[i]);
  }


  for(let i = 0; i < people.length; i++){
      if(people[i] > priceBillet && balance < people[i]-priceBillet){
          return 'NO';
      } else{
          if(people[i] > priceBillet){
              balance = balance + people[i] - priceBillet;
          } else{
              balance += priceBillet;
          }
      }
  }

  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
